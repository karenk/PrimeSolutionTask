<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'prime-solutions');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'R1p@f{(Ro_),r7PN{JGG;$9Q8Q[WAAg^#4&dp}`:~u<5So^&T m*~U6pnLla1|%)');
define('SECURE_AUTH_KEY',  'vbXeI|tD[AtwaF{u1,TuZZC},^])}*ipXm(_7%?c.gurxi~3eVEmE7l>!i<A$xg3');
define('LOGGED_IN_KEY',    'j8(A*P0.`BPj9Y&!f ^~@aLWq@W)CN{6VQwgX?ejo#]}E4wjm?_l6TyU+u;08D4[');
define('NONCE_KEY',        'oD*<^YmD,<D+TMrgd7U2NHFgY3tj5^3+N!,Kt(7.gd/jA3A4,-Z kvr[?jh2P0)F');
define('AUTH_SALT',        'n?b23jDawJ_Sp^*%hgW(E/-KsX%<>p06=a4R^4^w9~A0%o|8DU|S34&,rftpGp62');
define('SECURE_AUTH_SALT', 'D*auBHn4]XV9fs{x(@p0VMZq!ji&z7HHJf B]8MJaPp>qT.5+0)^H|RMpa=Pc{.F');
define('LOGGED_IN_SALT',   '<!@KP+{Kw@*QS$ugH;XL6E}/ [sn3NlI{3nZF)#gLH<81U=Ve*{5;{ht3!]Z.@S[');
define('NONCE_SALT',       'aeLRYs62,xpa/M>~AJD_)|w^59qo`Xm=Oh+G 6z^Tq8g@^zr9JC%-e%#$;vx2Xux');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
