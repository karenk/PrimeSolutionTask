<?php

/**
 * Created by PhpStorm.
 * User: karko
 * Date: 04.07.2016
 * Time: 06:08
 */
class Image
{
    private $src;
    private $author;
    private $author_id;
    private $date_taken;
    private $description;
    private $link;
    private $published;
    private $tags;
    private $title;

    /**
     * Image constructor.
     * @param $src
     * @param $author
     * @param $author_id
     * @param $date_taken
     * @param $description
     * @param $link
     * @param $published
     * @param $tags
     * @param $title
     */
    public function __construct($src, $author, $author_id, $date_taken, $description, $link, $published, $tags, $title)
    {
        $this->src = $src;
        $this->author = $author;
        $this->author_id = $author_id;
        $this->date_taken = $date_taken;
        $this->description = $description;
        $this->link = $link;
        $this->published = $published;
        $this->tags = $tags;
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getSrc()
    {
        return $this->src;
    }

    /**
     * @param mixed $src
     */
    public function setSrc($src)
    {
        $this->src = $src;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getAuthorId()
    {
        return $this->author_id;
    }

    /**
     * @param mixed $author_id
     */
    public function setAuthorId($author_id)
    {
        $this->author_id = $author_id;
    }

    /**
     * @return mixed
     */
    public function getDateTaken()
    {
        return $this->date_taken;
    }

    /**
     * @param mixed $date_taken
     */
    public function setDateTaken($date_taken)
    {
        $this->date_taken = $date_taken;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * @param mixed $published
     */
    public function setPublished($published)
    {
        $this->published = $published;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function toMap()
    {
        return array(
            'src' => $this->src,
            'author' => $this->author,
            'author_id' => $this->author_id,
            'date_taken' => $this->date_taken,
            'description' => $this->description,
            'link' => $this->link,
            'published' => $this->published,
            'tags' => $this->tags,
            'title' => $this->title
        );
    }

}