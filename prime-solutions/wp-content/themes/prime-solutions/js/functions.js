$(document).ready(function () {
    loadImages();
    $(".load-images-btn").on("click", handleSearchBtn);
    $(".save-data-btn").on("click", handleSaveDataBtn);
});//End Ready

function jsonFlickrFeed(json) {
    console.log(json);

    $.each(json.items, function (i, item) {
        $("<div>").addClass("item-" + i).addClass("item").prependTo("#images-all");
        var jImg = $("<img />").appendTo("#images-all .item-" + i);
        jImg.attr("src", item.media.m);
        jImg.attr("data-author", item.author);
        jImg.attr("data-author_id", item.author_id);
        jImg.attr("data-date_taken", item.date_taken);
        jImg.attr("data-description", item.description);
        jImg.attr("data-link", item.link);
        jImg.attr("data-published", item.published);
        jImg.attr("data-tags", item.tags);
        jImg.attr("data-title", item.title);
    });
}

function handleSearchBtn() {
    $("#images-all").html('<h2>All results</h2>');
    var data = $(".search-form").serializeObject();

    $.ajax({
        url: 'https://api.flickr.com/services/feeds/photos_public.gne',
        dataType: 'jsonp',
        data: {
            "tags": data.search,
            "format": "json"
        }
    });
}

function updateImages(json) {
    for (var i in json) {
        var item = json[i];
        $("<div>").addClass("item-" + i).addClass("item").prependTo("#images-all");
        var jImg = $("<img />").appendTo("#images-all .item-" + i);
        jImg.attr("src", item.src);
        jImg.attr("data-author", item.author);
        jImg.attr("data-author_id", item.author_id);
        jImg.attr("data-date_taken", item.date_taken);
        jImg.attr("data-description", item.description);
        jImg.attr("data-link", item.link);
        jImg.attr("data-published", item.published);
        jImg.attr("data-tags", item.tags);
        jImg.attr("data-title", item.title);
    }
}
function loadImages() {
    $.ajax({
        url: getAjaxUrl(),
        method: 'GET',
        data: {
            action: 'get_images_list',
            table: 'all_images'
        },
        success: function (data) {
            // console.log('success' + data);
            var json = JSON.parse(data);
            updateImages(json);
        },
        error: function (errorThrown) {
            console.log('error' + errorThrown);
        }
    });
}

function getImagesList(jParent) {
    var imagesList = [];
    jParent.find(".item img").each(function () {
        var image = {};
        image.src = $(this).attr("src");
        image.author = $(this).attr("data-author");
        image.author_id = $(this).attr("data-author_id");
        image.date_taken = $(this).attr("data-date_taken");
        image.description = $(this).attr("data-description");
        image.link = $(this).attr("data-link");
        image.published = $(this).attr("data-published");
        image.tags = $(this).attr("data-tags");
        image.title = $(this).attr("data-title");
        imagesList.push(image);
    });
    return imagesList;
}

function getAjaxUrl() {
    var http = location.protocol;
    var slashes = http.concat("//");
    var host = slashes.concat(window.location.hostname);
    return host + "/prime-solutions/wp-admin/admin-ajax.php";
}
function handleSaveDataBtn() {
    var imagesList = getImagesList($("#images-all"));
    // console.log(JSON.stringify(imagesList));
    var finalData = JSON.stringify(imagesList).replace(/\\/g, "");
    // console.log(finalData);
    $.ajax({
        url: getAjaxUrl(),
        method: 'POST',
        data: {
            action: 'save_images_list',
            images_list: JSON.stringify(imagesList),
            table: 'all_images'
        },
        success: function (data) {
            // This outputs the result of the ajax request
            console.log('success' + data);
        },
        error: function (errorThrown) {
            console.log('error' + errorThrown);
        }
    });
    console.log(imagesList);
}

$.fn.serializeObject = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};