<!Doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen"/>

    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>

    <script src="<?php bloginfo('template_directory'); ?>/jquery-ui/jquery-ui.js"></script>
    <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/jquery-ui/jquery-ui.css">
    <style>
        #draggable, #draggable2 {
            width: 100px;
            height: 100px;
            padding: 0.5em;
            float: left;
            margin: 10px 10px 10px 0;
        }

        #droppable {
            width: 150px;
            height: 150px;
            padding: 0.5em;
            float: left;
            margin: 10px;
        }
    </style>
    <script>
        $(function () {
            $("#draggable").draggable({revert: "valid"});
            $("#draggable2").draggable({revert: "invalid"});

            $("#droppable").droppable({
//                activeClass: "ui-state-default",
//                hoverClass: "ui-state-hover",
                drop: function (event, ui) {
//                    $( this )
//                        .addClass( "ui-state-highlight" )
//                        .find( "p" )
//                        .html( "Dropped!" );
//                    $( "#draggable2" ).draggable({ revert: "valid" });
                }
            });
        });
    </script>
</head>
<body>
<div class="wrapper">
    <div class="container">
        <div class="search-form-container">
            <form class="search-form">
                <input name="search" type="text" placeholder="Search"/>
                <a class="load-images-btn btn chocolate-bg">Load images</a>
                <a class="save-data-btn btn green-bg">Save data</a>
            </form>
        </div>
        <div class="search-results">
            <div id="images-all" class="images-all">
            </div>

            <div id="draggable" class="ui-widget-content">
                <p>I revert when I'm dropped</p>
            </div>

            <div id="draggable2" class="ui-widget-content">
                <p>I revert when I'm not dropped</p>
            </div>

            <div id="droppable" class="ui-widget-header">
                <p>Drop me here</p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="<?php bloginfo('template_directory') ?>/js/functions.js"></script>
</body>
</html>
