<?php
include 'classes/Image.php';
/**
 *
 */
function save_images_list()
{
    if (isset($_POST)) {
        dropTable($_POST['table']);
        createTable($_POST['table']);
        $imagesListStr = $_POST['images_list'];
        $imagesListJson = json_decode(stripslashes($imagesListStr));
        foreach ($imagesListJson as $item) {
            $image = new Image(
                $item->src,
                $item->author,
                $item->author_id,
                $item->date_taken,
                $item->description,
                $item->link,
                $item->published,
                $item->tags,
                $item->title
            );
            insertImage($_POST['table'], $image);
        }
    }
    die();
}

add_action('wp_ajax_save_images_list', 'save_images_list');
add_action('wp_ajax_nopriv_save_images_list', 'save_images_list');

function get_images_list()
{
    if (isset($_GET)) {
        global $wpdb;
        $sql = "SELECT * FROM " . $wpdb->prefix . $_GET['table'] . ";";
        $result = $wpdb->get_results($sql);
        print json_encode($result);
    }
    die();
}

add_action('wp_ajax_get_images_list', 'get_images_list');
add_action('wp_ajax_nopriv_get_images_list', 'get_images_list');

function createTable($table_name)
{
    global $wpdb;
    $sql = "CREATE TABLE IF NOT EXISTS " . $wpdb->prefix . $table_name . " (
            id int(10) NOT NULL AUTO_INCREMENT,
            src VARCHAR(200) NOT NULL,
            author VARCHAR(200) NOT NULL,
            author_id VARCHAR(200) NOT NULL,
            date_taken VARCHAR(200) NOT NULL,
            description VARCHAR(1000) NOT NULL,
            link VARCHAR(200) NOT NULL,
            published VARCHAR(200) NOT NULL,
            tags VARCHAR(200) NOT NULL,
            title VARCHAR(200) NOT NULL,
            PRIMARY KEY (id)
    );";
    $wpdb->query($sql);
}

function dropTable($table_name)
{
    global $wpdb;
    $sql = "DROP TABLE " . $wpdb->prefix . $table_name . ";";
    $wpdb->query($sql);
}

function insertImage($table_name, $image)
{
    global $wpdb;
    $wpdb->insert(
        $wpdb->prefix . $table_name,
        $image->toMap()
    );
}